package Game;

import java.util.ArrayList;

public class AIPlayer extends Player{

	//VARIABILE CHE INDICA IL LIVELLO DI DIFFICOLTA' DEL GIOCO, E QUINDI IL LIVELLO DI "INTELLIGENZA" DELL'AI
	//VA DA 1 A 3; 1 E' PRINCIPIANTE, 2 E' MEDIO, 3 E' DIFFICILE.
	private int difficult;
	
	//QUESTO PARAMETRO PERMETTE ALL'AI DI SAPERE TUTTO SUI GIOCATORI, SUI MAZZI E SUL MAZZO PRINCIPALE 
	private Table data;
	
	
	
	//COSTRUITO UN GIOCATORE IN BASE AL SUO INDICE, QUESTI SARANNO TUTTI GIOCATORI PILOTATI DALL'AI
	public AIPlayer(int index, int difficult, Table data) {
		String i = String.valueOf(index);
		this.nickname = "Giocatore_" + i;
		this.points = 0;
		this.location = index;
		this.order = index;
		this.difficult = difficult;
		this.data = data;
	}
	
	
	
	//Get and Set of difficult attribute
	public int getDifficult() {
		return difficult;
	}
	public void setDifficult(int difficult) {
		this.difficult = difficult;
	}


	
	/* QUESTA FUNZIONE PERMETTE DI AGGIORNARE LA TAVOLA VISTA DALL'AI IN MODO DA POTER FARE LA SCELTA OTTIMALE.
	 * OVVIAMENTE VEDRA' SOLTANTO QUELLO GIOCATO FINO AL SUO TURNO, E NON TUTTO QUELLO NELLA MANO DELGI AVVERSARI */
	public void refreshData (Table data) {
		this.data = data;
	}
	
	
	/*NEL CASO DI DIFFICOLTA' UGUALE A 1, L'AI SCEGLIE CASUALMENTE UNA CARTA DA GIOCARE
	 * NEL CASO DI DIFFICOLTA' UGUALE A 2, L'AI SCEGLIE IN BASE ALLE CARTE CHE CI SONO SUL CAMPO
	 * NEL CASO DI DIFFICOLTA' UGUALE A 3, L'AI SCEGLIE IN BASE ALLE CARTE CHE CI SONO SUL CAMPO E A QUELLE CHE SONO ANDATE VIA
	*/
	@Override
	public int selectCard() {
		/* POS E' LA VARIABILE CHE MI DENTIFICA LA POSIZIONE DELLA CARTA DA GIOCARE */
		int pos = 0;
		
		switch(difficult) {
		
		case 1:
			/* MUL E' UN NUMERO CHE MI SERVE DA MOLTIPLICATORE PER IL NUMERO RANDOM, IN MODO CHE NON POSSO SCEGLIERE
			 * LA CARTA 3 SE QUESTA NON ESISTE */
			int mul = 3;
			if ((hand.size() == 1) || (hand.size() == 0))
				mul -= 2;
			if (hand.size() == 2)
				mul -= 1;
			pos = (new Float(Math.floor(Math.random() * mul))).intValue();
			break;
				
			
			
		case 2:
			/* QUI ORA DOVREI FARE TUTTA UNA SERIE DI IF PER CAPIRE CHE CARTE CI SONO ORA SUL TAVOLO E COME GIOCARE */
			/* ORA IN DATA.PLATE HO TUTTE LE CARTE SUL TAVOLO FINO AD ORA (MOMENTO IN CUI DEVO GIOCARE), QUINDI SE SONO IL
			 * TERZO GIOCATORE (PER ESEMPIO) , CI SARANNO 2 CARTE SUL PIATTO */
			
			/* SETTO DELLE VARIABILI DI APPOGGIO, IN CUI SALVERO' LA PRIMA CARTA GIOCATA DELLA TAVOLA */
			boolean checkBriscola = false;
			boolean checkCarico = false;
			Card MaxBriscola = new Card();
			Card first = new Card(data.getPlate().get(0).getSeed(), data.getPlate().get(0).getValue());
			
			//1^ CONTROLLO : CONTROLLO SE CI SONO BRISCOLE E SE CI SONO, MI SALVO LA PIU' POTENTE.
			// CONTROLLO ANCHE SE CI SONO DEI CARICHI 
			for (Card c : data.getPlate()) {
				if (c.getSeed().equals(data.getBriscola().getSeed())) {
					checkBriscola = true;
					MaxBriscola.setSeed(c.getSeed());
					if (data.getBriscola().powerValue() > MaxBriscola.powerValue()) 
						MaxBriscola.setValue(data.getBriscola().getValue());
				}
				if ((c.pointValue() >= 10) && !(c.getSeed().equals(data.getBriscola().getSeed()))) {
					checkCarico = true;
				}
			}
			
			/* 2^ CONTROLLO: SE SONO L'ULTIMO GIOCATORE DELLA MANO */
			if (data.getPlate().size() == 3) {
				/* 2.1 SE POSSO PRENDERE E NON CI SONO BRISCOLE, PRENDO SENZA BRISCOLA*/
				for (Card c : hand) {
					if (c.getSeed().equals(first.getSeed()) && !checkBriscola) {
						if (c.powerValue() > first.powerValue())
							pos = hand.indexOf(c);
					}
				}
			}
			
			break;
			
			
			
		case 3:
			break;
		
		}
		
		return pos;
	}
	
	
	@Override
	public String toString() {
		return "AIPlayer [nickname=" + nickname + ", points=" + points + ", location=" + location + ", order=" + order
				+ "]";
	}
}
