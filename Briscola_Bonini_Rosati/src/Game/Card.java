package Game;

public class Card {
	//That's the seed of the card
	private String seed;
	//That's the value of the card
	private int value;

	
	
	//The constructor of the card creates the card
	public Card(){
		this.value = 0;
	}
	//The constructor of the card creates the card, from a seed and a value
	public Card(String seed, int value){
		this.seed = seed;
		this.value = value;
	}

	
	
	//Get and Set of seed attribute
	public String getSeed() {
		return seed;
	}
	public void setSeed(String seed) {
		this.seed = seed;
	}
	//Get and Set of value attribute
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	
	
	//QUESTO METODO MI PERMETTE DI RITORNARE UN NUMERO CHE EQUIVALE ALLA POTENZA DELLA CARTA. PI� � ALTA LA SUA POSIZIONE NELL'ARRAY
	//DEI VALORI, PI� QUELLA CARTA � FORTE
	public int powerValue() {
		int pos = 0;
		for(int i = 0; i < 10; i++) {
			if (Value.numbers[i] == value) {
				pos = i;
			}
		}
		return pos;
	}
	
	
	//That method return the counting value of a card
	public int pointValue() {
		if(value < 8 && value != 1 && value != 3) {
			return 0;
		} else if(value > 7) {
			return (value - 6);
		} else if(value == 3) {
			return 10;
		} 
		return 11;
	}
	
	
	//The method that allows a card to print itself
	@Override
	public String toString() {
		return "Card [seed=" + seed + ", value=" + value + "]";
	}
	
}
