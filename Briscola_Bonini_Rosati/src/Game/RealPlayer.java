package Game;

import java.util.Scanner;

public class RealPlayer extends Player{

	//Costruisco un giocatore vuoto
	public RealPlayer() {
	}
	//COSTRUISCO UN GIOCATORE IN BASE 
	public RealPlayer(String nickname) {
		this.nickname = nickname;
		this.points = 0;
		this.location = 1;
		this.order = 1;
	}
	
	
	
	/*QUESTO METODO MI PERMETTE DI SELEZIONARE UNA CARTA DALLA MANO DEL GIOCATORE REALE TRAMITE TASTIERA*/
	public  int selezioneProvvisoria(Scanner s1) {
		int n;
		while(true) {
			System.out.println("SELEZIONA LA CARTA DA GIOCARE");
			n = s1.nextInt();
			if (n < 0 || n > 2)
				System.out.println("NUMERO ERRATO!!! INSERIRE UN NUMERO TRA 0 E 2 COMPRESI");
			else
				break;
		}
		return n;
	}
	
	
	//Per selezionare la carta, devo avere l'interfaccia grafica
	@Override
	public int selectCard() {
		int number = -1;
		number = selezioneProvvisoria(new Scanner(System.in));
		return number;
	}

	
	@Override
	public String toString() {
		return "RealPlayer [nickname=" + nickname + ", points=" + points + ", location=" + location + ", order=" + order
				+ "]";
	}
}
