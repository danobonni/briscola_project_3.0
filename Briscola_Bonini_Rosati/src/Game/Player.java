package Game;

import java.util.ArrayList;
import java.util.HashSet;

public abstract class Player {
	//That attribute represents the name of the player
	protected String nickname;
	//That attribute represents cards in the hand of a player
	protected ArrayList<Card> hand = new ArrayList<Card>();
	//That attribute represents cards that a player has won in a turn
	protected HashSet<Card> deckPlayer = new HashSet<Card>();
	//That attribute represents the points of a player
	protected int points;
	//That attribute represents the location of a player
	protected int location;
	//That attribute represents the order of a player
	protected int order;

	
	
	//Get and Set of nickname attribute
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	//Get and Set of hand attribute
	public ArrayList<Card> getHand() {
		return hand;
	}
	public void setHand(ArrayList<Card> hand) {
		this.hand = hand;
	}
	//Get and Set of deckPlayer attribute
	public HashSet<Card> getDeckPlayer() {
		return deckPlayer;
	}
	public void setDeckPlayer(HashSet<Card> deckPlayer) {
		this.deckPlayer = deckPlayer;
	}
	//Get and Set of points attribute
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	//Get and Set of location attribute
	public int getLocation() {
		return location;
	}
	public void setLocation(int location) {
		this.location = location;
	}
	//Get and Set of order attribute
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}

	

	//QUESTO METODO MI DICE QUALE CARTA METTERE IN CAMPO, VERR� POI UTILIZZATO DALLA PUTCARD, ed steso ai suoi figli, OGNUNO
	//IL QUALE SAR� O PILOTATO DAL PC OPPURE DA U GIOCATORE FISICO
	public abstract int selectCard();
		
	
	/*SONO TRANQUILLO CHE POSSO METTERE SEMPRE CARTE, ERCH� SE UN GIOCATORE NON AVESSE CARTE IN MANO, IL GIOCO FINIREBBE */
	/*In this method, the player put a card from the hand to the table, if the hand is not empty, and remove them from the hand. 
	 * If the hand is empty, an exception is launched and the method don't do anything to the hand or the table*/
	public void putCard(Table t, int index){
		ArrayList<Card> tmp = new ArrayList<Card>(t.getPlate());
		tmp.add(hand.get(index));
		t.setPlate(tmp);
		hand.remove(index);
	}
	
	
	/*In this method, the player draw a card form the deck, if the deck is not empty, and remove the card from the main deck.
     * If the deck is empty, an exception is launched and the method don't do anything to the deck or the hand*/
    public void drawCard(Deck d) throws EmptyDeckException {
    	ArrayList<Card> tmp;
        if (d.getStack().isEmpty()) {
            throw new EmptyDeckException();
        } else {
            hand.add(d.getStack().get(0));
            tmp = new ArrayList<Card>(d.getStack());
            tmp.remove(0);
            d.setStack(tmp);
        }
    }
	
    
	//This method allow to show the Player's hand
	public void showHand() {
		for(Card c : hand) {
			System.out.println(c.toString());
		}
	}
	
	//This method allow to show the Player's hand
	public void showDeckPlayer() {
		for(Card c : deckPlayer) {
			System.out.println(c.toString());
		}
	}
	
	
}
