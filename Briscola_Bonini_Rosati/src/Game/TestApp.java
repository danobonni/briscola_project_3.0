package Game;
/*	
 * 	IL GIOCO E' FUNZIONANTE XD!!! PER GIOCARE FARE PARTIRE IL GAME, ANDARE NELLA CONSOLE E QUANDO IL GIOCO E' IN PAUSA DIGITARE
 * 	DA 0 A 2 (INDICE DI CARTA IN MAN0) PER SELEZIONARE LA CARTA CHE SI VUOLE GIOCARE DALLA MANO DEL GIOCATORE.
 * 	BISOGNA GIOCARE PER TUTTI E DUE I GIOCATORI.
 * 
 * 	1)CREARE PLAYER COME CLASSE ASTRATTA AVENTE DUE FIGLI REALI: AI E REAL-PLAYER. L'UNICA COSA ASTRATTA DI PLAYER E' IL METODO
 * 		PUTCARD, DA IMPLEMENTARE NEI SUI FIGLI.
 * 	2)L'AI POTREBBE AVERE DUE FILI????? FRIENDLY ED ENEMY!
 * 	3)OTTIMIZZARE LE FUNZIONI DI VINCITA DELLA MANO, VINCITA DELLA PARTITA E ORDINAMENTO DEI GIOCATORI
 * 	4)SCRIVERE UNA FUNZIONE PER SCEGLIERE LA CARTA DALLA MANO
 * 	5)SCRIVERE I COMMENTI FATTI BENE
 * 	6)AGGIUNGERE LA PARTE GRAFICA AL GIOCO
 * 	7)STUDIARE E INTEGRARE SQLITE PER SALVARE I RECORD DELLE PARTITE
 * */

import java.util.Scanner;



public class TestApp {
	
	
	public static void main(String[] args){
		
		int nPlayer=0, livDiff=0, n;
		Deck d = new Deck();
		RealPlayer p1 = new RealPlayer("Bonny");
		Player p2 = new RealPlayer();
		Scanner s = new Scanner(System.in);
		
		/* ******************	FASE DI IMPOSTAZIONE DEL GIOCO ***************** */
		while (true) {
			System.out.println("SELEZIONA IL NUMERO DI GIOCATORI");
			nPlayer = s.nextInt();
			if (nPlayer <= 1 || nPlayer > 4)
				System.out.println("NUMERO ERRATO!!!2 INSERIRE UN NUMERO TRA 2 E 4 COMPRESI");
			else
				break;
		}
		while (true) {
			System.out.println("SELEZIONA IL LIVELLO DI DIFFICOLTA'");
			livDiff = s.nextInt();
			if (livDiff <= 0 || livDiff > 3)
				System.out.println("NUMERO ERRATO!!! INSERIRE UN NUMERO TRA 1 E 3 COMPRESI");
			else
				break;
		}
		
		/* Istanzio la classe table */
		Table t = Table.getInstance();
		
		t.create(p1, nPlayer, livDiff);
		
		
		/* ********************* FASE INIZIALE DI GIOCO *************************** */
		System.out.println(Seed.type.length);
		System.out.println(Value.numbers.length);
		System.out.println(d.getStack().size());
		System.out.println();
		System.out.println("Mazzo NON mescolato: contiene " + d.getStack().size() + " carte");
		d.showDeck();
		
		t.shuffle(d);
		System.out.println();
		System.out.println("Mazzo mescolato: contiene " + d.getStack().size() + " carte");
		d.showDeck();
		
		//t.sortPlayer(true, new Player());
		
		System.out.println();
		System.out.println("Briscola:");
		t.pickBriscola(d);
		
		t.distributeCards(d);
		System.out.println();
		System.out.println("Mazzo dopo aver tolto la mano iniziale: contiene " + d.getStack().size() + " carte");
		d.showDeck();
		
		for (Player p : t.getParticipants()) {
			System.out.println();
			System.out.println(p);
			System.out.println("Mano iniziale del giocatore:");
			p.showHand();
			System.out.println("Mazzo iniziale del giocatore:");
			p.showDeckPlayer();
		}
		System.out.println(t.getBriscola());
		
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("	FASE DI GIOCO");
		System.out.println();
		System.out.println();
		System.out.println();
		
		
		// ***************** FASE DI GIOCO VERO E PROPRIO ********************** 
		
		//IL ESEGUE UN FOR INFINITO, FINCHE� NON VIENE LANCIATA L'ECCEZZIONE CHE I GIOCATORI NON HANNO PI� CARTE IN MANO
		//SE I GIOCATORI NON HANNO PI� CARTE IN MANO, IL GIOCO � FINITO E MOSTRO IL GIOCATORE VINCENTE

		//CONTINUO A GIOCARE FINO A CHE I GIOCATORE 1, MA E' UGUALE PER TUTTI I GIOCATORI, NON HA FINITO LE CARTE IN MANO
		//SE NON HA PIU' CARTE IN MANO ALLORA HO FINITO IL GIOCO
		int i = 0;
		while (p1.getHand().isEmpty() == false)
		{
			//MOSTRO QUANTE CARTE CONTIENE IL MAZZO
			System.out.println();
			System.out.println("Mazzo dopo aver tolto la " + (i+1) + "-esima mano: contiene " + d.getStack().size() + " carte");
			System.out.println();
			
			//FACCIO VEDERE LA BRISCOLA
			System.out.println();
			System.out.print("Briscola:	");
			System.out.print(t.getBriscola());
			System.out.println();
			
			//FACCIO VEDERE LA MANO DI TUTTI I GIOCATORI
			for (Player p : t.getParticipants()) {
				System.out.println();
				System.out.print("Mano del giocatore:	");
				System.out.print(p);
				System.out.println();
				p.showHand();
			}
					
			//FACCIO VEDERE IL PIATTO, OVVERO LE CARTE CHE I GIOCATORI HANNO TIRATO (OVVVIAMENTE LE SCELGNONO)
			System.out.println();
			System.out.println("Piatto:");
			for(Player p : t.getParticipants()) {
				System.out.println(p);
				/* PRIMA CHE IL GIOCATORE SCELGA UNA CARTA, SE E' UN AI!!!,DEVO REFRESHARE I DATI DELL'AI, IN MODO
				 * CHE POSSA VEDERE QUALI CARTE SONO STATE GIOCATE FINO AD ORA */
				if (p instanceof AIPlayer)
					((AIPlayer) p).refreshData(t);
				
				n = p.selectCard();
				if (! p.getHand().isEmpty()) {
					System.out.println(p.getHand().get(n));
				}
				p.putCard(t, n);
			}
			
			//FACCIO VEDERE LA MANO, DOPO CHE I GIOCATORI HANNO MESSO LA CARTA
			for (Player p : t.getParticipants()) {
				System.out.println();
				System.out.print("Mano del giocatore:	");
				System.out.print(p);
				System.out.println();
				p.showHand();
			}
					
			//FACCIO VEDERE CHI VINCE LA MANO
			n = t.winnerOfPlate();
			for(Player p : t.getParticipants()) {
				if (p.getOrder() == n)
					p2 = p;
			}
					
			//ORDINO I GIOCATORI, IN BASE AL VINCENTE DELLA MANO
			t.sortPlayer(false, p2);
					
			//AGGIUNGO IL PIATTO AL MAZZO DEL GIOCATORE CHE HA VINTO LA MANO
			t.addDeckPlayer(p2);
			
					
			//CONTO I PUNTI DI OGNI GIOCATORE 
			for(Player p : t.getParticipants()) {
				t.countPoints(p);
			}
					
			//MOSTRO TUTTI I GIOCATORI E CHI HA VINTO LA MANO
			System.out.println();
			System.out.println("Questa mano e' stata vinta da: " + p2);
			
			for(Player p : t.getParticipants()) {
				System.out.println();
				System.out.print("Mazzo del giocatore:	");
				System.out.print(p);
				System.out.println();
				p.showDeckPlayer();
			}
			
			try {
				//FACCIO PESCARE AD OGNI GIOCATORE UNA CARTA
				for(Player p : t.getParticipants()) {
					p.drawCard(d);
				}
			} catch (EmptyDeckException e) {
				//NESSUN GIOCATORE PESCA
			}
			
			
			i++;
		}
		
		//MOSTRO IL GIOCATORE VINCENTE ED ESCO
		n = t.winnerOfGame();
		for(Player p : t.getParticipants()) {
			if (p.getLocation() == n)
				p2 = p;
		}
		
		if(p2.equals(null)) {
			System.out.println();
			System.out.println();
			System.out.println("C'E' STATO UN PAREGGIO!");
		} else {
			System.out.println();
			System.out.println();
			System.out.println("HA VINTO IL GIOCATORE " + p2);
		}
		
		s.close();
		
	}
}
					