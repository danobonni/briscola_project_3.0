package Game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

public class Table {
    //Distribuisci carte
    //Estrai briscola
    //dichiara il vincitore della mano
    //dichiara il vincitore della partita

    //This attribute identifies the briscola's card
    private Card briscola;
    //That attribute represents cards on the table
    private ArrayList<Card> plate = new ArrayList<Card>();
    //That attribute represents players that join the game
    private ArrayList<Player> participants = new ArrayList<Player>();
    //That attribute represents the number of players that join the game.
    private int numberOfPlayer;
    //That attribute represents the initial number of cards in the hand of a player. In briscola is always 3
    private final int initialCardInHand = 3;

    /*QUESTE RIGHE DI CODICE PERMETTONO DI ISTANZIARE UNA SOLA VOLTA LA CLASSE TABLE, DOPO POSSO RICHIAMARE L'ISTANZA DI TABLE 
     * ATTRAVERSO IL METODO GETINSTANCE*/
    private static Table instance;
    
    private Table() {}
    
    public static Table getInstance() {
    	if (instance == null)
    		instance = new Table();
    	
    	return instance;
    }
    
    
    
    //LA TABELLA VIENE CREATA DA UN GIOCATORE FISICO, E TANTI AIPLAYER IN BASE AL NUMERO DI PARTECIPANTI. OGNUNO DEI QUALI AVR�
    //IL LIVELLO DI DIFFICOLT� IMPOSTATO A UN VALORE SCELTO DALL'UTENTE
    /*This constructor generate a table. The table is composed by numberOfPlayer players. The first player is the player created by
     * the Real Player. The others are created by the game.*/
    public void create(RealPlayer p1, int numberOfPlayer, int difficult) {
        participants.add(p1);
        this.numberOfPlayer = numberOfPlayer;
        for (int i = 1; i < numberOfPlayer; i++) {
            participants.add(new AIPlayer(i+1, difficult, this));
        }
    }

    
    
    //Get and Set of briscola attribute
    public Card getBriscola() {
		return briscola;
	}
	public void setBriscola(Card briscola) {
		this.briscola = briscola;
	}
	//Get and Set of plate attribute
	public ArrayList<Card> getPlate() {
		return plate;
	}
	public void setPlate(ArrayList<Card> plate) {
		this.plate = plate;
	}
	//Get and Set of participants attribute
	public ArrayList<Player> getParticipants() {
		return participants;
	}
	public void setParticipants(ArrayList<Player> participants) {
		this.participants = participants;
	}
	//Get and Set of numberOfPlayer attribute
	public int getNumberOfPlayer() {
		return numberOfPlayer;
	}
	public void setNumberOfPlayer(int numberOfPlayer) {
		this.numberOfPlayer = numberOfPlayer;
	}
	//Get of initialCardInHand attribute
	public int getInitialCardInHand() {
		return initialCardInHand;
	}



	//This method gives a shuffled deck, from a not mixed deck, and set the boolean mixed on true
    public void shuffle(Deck d) {
    	ArrayList<Card> tmp = new ArrayList<Card>(d.getStack());
        Collections.shuffle(tmp);
        d.setStack(tmp);
        d.setMixed(true);
    }


    //In this method, the player take the card from the table, put them to their own deck and remove them to the plate of the table
    public void addDeckPlayer(Player p) {
    	HashSet<Card> tmp = new HashSet<Card>(p.getDeckPlayer());
        tmp.addAll(plate);
        p.setDeckPlayer(tmp);
        plate.clear();
    }


    //This method allow the player to count their total own points
    public void countPoints(Player p) {
		p.setPoints(0);
		for (Card c : p.deckPlayer) 
		    p.setPoints(p.getPoints() + c.pointValue());
    }


    /*In this method, the table distribute the initial hand to all the player. That method can throws the EmptyDeckException, but that exception
     * will never be found, because the deck, at the start of the game, has surely more cards than the initial hand of all players.*/
    public void distributeCards(Deck d){
    	ArrayList<Card> tmp = new ArrayList<Card>();
        for (int i = 0; i < initialCardInHand; i++) {
            for (Player p : participants) {
            	tmp = p.getHand();
                tmp.add(d.getStack().get(0));
                p.setHand(tmp);
                tmp = d.getStack();
                tmp.remove(0);
                d.setStack(tmp);
            }
        }
    }


    //LA BRISCOLA � L'ULTIMA CARTA DEL MAZZO, COS� VIENE PESCATA NORMALMENTE DALL'ULTIMO GIOCATORE
    //In that method the table pick the Briscola card from the deck
    public void pickBriscola(Deck d) {
        briscola = d.getStack().get((Seed.type.length * Value.numbers.length) - 1);
    }


    //QUESTA FUNZIONE RITORNA UN NUMERO INTERO CHE MI INDICA LA POSTAZIONE DEL GIOCATORE CHE HA VINTO
    //SUBITO CONTROLLO CI SIA UNA BRISCOLA, SE C'�, PRENDO QUELLA PI� ALTA E SO CHE IL GIOCATORE I-ESIMO HA GIOCATO QUELLA BRISCOLA
    //ALTRIMENTI PRENDO IL SEME DELLA PRIMA CARTA GIOCATA E FACCIO LA STESSA COSA.
    //L'ORDINE DELLE CARTE NELL'ARRAY DI CARTE DEL PIATTO, � DATO DALL'ORDINE I CUI VENGONO MESSE DAI GIOCATORI. ES:
    //LA PRIMA CARTA DELL'ARRAY DEL PLATE, SAR� LA CARTA MESSA DAL GIOCATORE 1, OVVERO QUELLO CHE HA VINTO LA MANO PRIMA
    //FORSE C'� UN MODO PER NON RIPETERE LA PARTE DENTRO I FOR, E FARE UN CICLO UNICO
    public int winnerOfPlate() {
        int position = 0;
        Card maxValue = new Card();
        String s;
        /* CONTROLLO SE C'E' UNA BRISCOLA E SE C'� MI SALVO LA PIU' POTENTE */
        for (Card c : plate) {
            if (c.getSeed().equals(briscola.getSeed())) {
                if (c.powerValue() >= maxValue.powerValue()) {
                    position = plate.indexOf(c);
                    maxValue.setSeed(c.getSeed());
                    maxValue.setValue(c.getValue());
                }
            }
        }
        /* SE NON CI SONO BRISCOLE MI SALVO LA CARTA PIU' FORTE */
        if (maxValue.getValue() == 0) {
            s = plate.get(0).getSeed();
            for (Card c : plate) {
                if (c.getSeed().equals(s)) {
                    if (c.powerValue() >= maxValue.powerValue()) {
                        position = plate.indexOf(c);
                        maxValue.setSeed(c.getSeed());
                        maxValue.setValue(c.getValue());
                    }
                }
            }
        }
       return (position + 1);
    }

 
    //PROBLEMA:::::::::::::::
    //DA MODIFICARE IL TIPO DI RITORNO: CONSIDERARE SE MEGLIO TORNARE UN INT CHE IDENTIFICA LA LOCAZIONE DEL GIOCATORE CHE HA VINTO, 
    //OPPURE UN GIOCATORE
    
    //SE � LA PRIMA VOLTA CHE GIOCO (INIZIO DEL GAME), ALLORA ASSEGNO UN ORDINE CASUALE AI GIOCATORI, ALTRIMENTI IL PRIMO GIOCATORE A PARTIRE
    //� QUELLO CHE HA VINTO LA MANO PRECEDENTE. MA DEVO DECIDERE SE SETTARE L'ATTRIBUTO ORDER DEI PLAYER, OPPURE SE CAMBIARE L'ORDINE DEI
    //PLAYER NELL'ATTRIBUTO PARTICIPATS DELLA TABLE.
    //NEL MODO CON LA ROTATE, PRATICAMENTE RUOTO LA LISTA (RUOTARE DI 1 SIGNIFICA FARE UNO SHIFT DA SINISTRA A DESTRA).
    //NELLA VERSIONE ATTUALE DOVREBBE ANDARE, MA AL POSTO DEGLI F VORREI UNA FUNZIONE MATEMATICA.
    //That method set the order of the participants
    public void sortPlayer(boolean startGame, Player p1) {
        int first = 0;
        int i = 1;
        if (startGame) {
            first = (new Float(Math.ceil(Math.random() * numberOfPlayer))).intValue();
        } else {
            first = p1.getOrder();
        }     
        if(first == 1 || first == 3) {
           	first -= 1;
        } else if(first == 2) {
           	first += 1;
        } else {
          	first -= 3;
        }
        Collections.rotate(participants, first);
        for(Player p : participants) {
        	p.setOrder(i);
        	i++;
        }
    }
    
    
    //DA MODIFICARE IL TIPO DI RITORNO: CONSIDERARE SE MEGLIO TORNARE UN INT CHE IDENTIFICA LA LOCAZIONE DEL GIOCATORE CHE HA VINTO, 
    //OPPURE UN GIOCATORE
    
    //DA FARE!!!!!!!!!!!!!
    //RITORNA UN NUMERO UGUALE A QUELLO DELLA POSIZIONE DEL VINCITORE; 	RITORNA 0 SE C'� STATO UN PAREGGIO
    //In that method the table declare the winner of the game. If all player's has the same amount of points, the game ends in a tie
    public int winnerOfGame() {
        int winner = 0;
        int max = 0;
        int max2 = 0;
        if(numberOfPlayer != 4) {
        	for (Player p : participants) {
                if (p.getPoints() > max) {
                    winner = p.getLocation();
                    max = p.getPoints();
                }
            }
            if(numberOfPlayer == 2) {
                if(max == 60) {
                	winner = 0;
                } 
            } else {
                if(max == 40) {
                	winner = 0;
                } 
            }
        } else {
        	for (Player p : participants) {
                if((p.getLocation() % 2) != 0) {
                	max += p.getPoints();
                } else {
                	max2 += p.getPoints();
                }
            }
        	if(max > max2) {
        		winner = 1;
        	} else if(max < max2) {
        		winner = 2;
        	} else {
        		winner = 0;
        	}
        }
        
        return winner;
    }
}
