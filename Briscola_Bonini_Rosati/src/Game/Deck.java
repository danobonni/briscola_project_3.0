package Game;

import java.util.ArrayList;

public class Deck {
	//This attribute identifies the deck itself
	private ArrayList<Card> stack = new ArrayList<Card>();
	//This boolean notifies if the deck is mixed or not
	private boolean mixed;
	

	
	/*In this constructor for each Seed of the Card (Coppe, Denari, Bastoni e Spade) and for each value, 
	 * create a deck of (seed*value) different object of type card and set the boolean mixed on false*/
	public Deck() {
		for(int i = 0; i < Seed.type.length; i++) {
			for(int j = 0; j < Value.numbers.length; j++) {
				stack.add(new Card(Seed.type[i], Value.numbers[j]));
			}
		}
		mixed = false;
	}
	
	
	
	//Get and Set of stack attribute
	public ArrayList<Card> getStack() {
		return stack;
	}
	public void setStack(ArrayList<Card> stack) {
		this.stack = stack;
	}
	//Get and Set of mixed attribute
	public boolean isMixed() {
		return mixed;
	}
	public void setMixed(boolean mixed) {
		this.mixed = mixed;
	}



	//This method allow to show the deck
	public void showDeck() {
		System.out.println("Mixed = " + mixed);
		for(Card c : stack) {
			System.out.println(c.toString());
		}
	}
	
}
